const Airtable = require('airtable');
const fs = require('fs');
require('dotenv');
require('dotenv').config();
let results = {};
var base = new Airtable({ apiKey: process.env.GUS_KEY }).base(process.env.GUS_DB);

base('Marqueurs').select({
    // Selecting the first 50 records in Grid view:
    maxRecords: 50,
    view: "Grid view"
}).eachPage(function page(records, fetchNextPage) {
    // This function (`page`) will get called for each page of records.

    records.forEach(function (record) {
        console.log('Retrieved', record.get('Nom'));
        let result = {
            "Nom": record.get('Nom'), 
            "Adresse": (record.get('Adresse') != undefined) ? record.get('Adresse') : "", 
            "Horaires": (record.get('Horaires') != undefined) ? record.get('Horaires') : "",
            "Longitude": +record.get('Longitude'),
            "Latitude": +record.get('Latitude'),
            "Telephone1": (record.get('Telephone1') != undefined) ? record.get('Telephone1') : "",
            "Web": (record.get('Web') != undefined) ? record.get('Web') : "",
        }
        if (!results[record.get('Categorie')]) {
            results[record.get('Categorie')] = [result];
        } else {
            results[record.get('Categorie')].push(result);
        }
    });

    fetchNextPage();

}, function done(err) {
    if (err) { console.error(err); return; }
    let data = JSON.stringify(results);
    try {
        const storeData = fs.writeFileSync('markers_airtable.json', data);
        //file written successfully
    } catch (err) {
        console.error(err)
    }
});
