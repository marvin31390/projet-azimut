


//Initialisation de la carte
//13 = zoom par défaut de la carte
var carte = L.map('maCarte').setView([43.108887, 0.724588], 13);

//Lien vers la source des données
L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
	attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
	minZoom: 1,
	maxZoom: 20
}).addTo(carte);

//forme de l'icone
var LeafIcon = L.Icon.extend({
	options: {
		iconSize: [50, 50],
		iconAnchor: [25, 50],
		popupAnchor: [-3, -76]
	}
});
//couleur 
var icons = {
	"hebergement": new LeafIcon({ iconUrl: 'icons/violet.png' }),
	"soigner": new LeafIcon({ iconUrl: 'icons/vert.png' }),
	"habiller": new LeafIcon({ iconUrl: 'icons/orange.png' }),
	"domicile": new LeafIcon({ iconUrl: 'icons/jaune.png' }),
	"laver": new LeafIcon({ iconUrl: 'icons/bleu.png' }),
	"manger": new LeafIcon({ iconUrl: 'icons/marron.png' }),
	"infos": new LeafIcon({ iconUrl: 'icons/noir.png' })
};

L.icon = function (options) {
	return new L.Icon(options);
};

var markers = [];
var markers_data;
// On récupère le contenu du fichier markers.json
// Celui-ci contient les coordonnées des marqueurs, divisés en groupes
var requete = new XMLHttpRequest();
requete.open("GET", "markers_airtable.json", false);
requete.send();
if (requete.readyState == 4 && requete.status == 200) {
	markers_data = JSON.parse(requete.responseText);
}
for (var property in markers_data) {
	window[property] = "";
	// Pour chaque groupe, on crée les marqueurs qu'on ajoute dans un layerGroup
	if (markers_data.hasOwnProperty(property)) {
		markers_data[property].map(data => {
			markers.push(L.marker([data.Latitude, data.Longitude], { icon: icons[property] }).bindPopup(`<h3>${data.Nom}</h3>${data.Adresse}<p>${data.Horaires}</p> <p><a href='tel:${data.Telephone1}'><i class='fas fa-phone'></i> ${data.Telephone1}</a> <p><a href='${data.Web}'><i class='fas fa-link'></i> ${data.Web}</a>  <p><button onclick="direction(${data.Latitude}, ${data.Longitude})">Go!</button>`));
		});
		window[property] = L.layerGroup(markers);
		markers = [];
	}
}

function afficherMarqueurs(nom) {
	clearLayers();
	console.log(window[nom]);
	carte.addLayer(window[nom]);
}

// Si URL en url#untruc, on récupère la layer qui s'appelle "untruc"
if (window.location.hash) {
	clearLayers()
	// On récupère dans l'url (en enlevant le premier caractère (#))
	var hash = window.location.hash.substr(1);
	// window["toto"] = variable qui s'appelle toto
	carte.addLayer(window[hash])
} else {
	afficherTout();
}

function afficherTout() {
	clearLayers()
	for (var property in markers_data) {
		carte.addLayer(window[property]);
	}
}

function clearLayers() {
	carte.eachLayer(function (layer) {
		carte.removeLayer(layer);
	})


	L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
		attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
		minZoom: 1,
		maxZoom: 20
	}).addTo(carte);
}

// Pour suivre le trajet de l'utilisateur : watchPosition
// https://developer.mozilla.org/fr/docs/Web/API/Geolocation/watchPosition
// Pour les itinéraires, on s'appuie sur Leaflet-routing-machine. 
// Les infos sont passées au choix par OSRM 
// (serveur de démo instable mais instructions en français)
// ou Mapbox (serveur pro et gratuit mais instructions en anglais)
// Pour l'instant, c'est plus simple de passer par OSRM
function direction(lat, lon) {
	if ("geolocation" in navigator) {
		// Refactoring : prévoir un fichier JS avec toutes les constantes
		let stored_locale = localStorage.getItem("i18n_locale");
		let locale = stored_locale ? stored_locale : "fr";
		navigator.geolocation.getCurrentPosition((position) => {
			L.Routing.control({
				language: locale,
				//router: L.Routing.mapbox('pk.eyJ1IjoibGF1ZGV2IiwiYSI6ImNrN2J2aGxybDA1ZHIzaGxjMmZvNXNsanIifQ.buFHRBDj_UYOFNPHKNd5lw'),
				waypoints: [
					L.latLng(position.coords.latitude, position.coords.longitude),
					L.latLng(lat, lon)
				]
			}).addTo(carte);

		})
		// navigator.geolocation.watchPosition((position) => {
		// 	clearLayers();
		// 	L.Routing.control({
		// 		language: locale,
		// 		//router: L.Routing.mapbox('pk.eyJ1IjoibGF1ZGV2IiwiYSI6ImNrN2J2aGxybDA1ZHIzaGxjMmZvNXNsanIifQ.buFHRBDj_UYOFNPHKNd5lw'),
		// 		waypoints: [
		// 			L.latLng(position.coords.latitude, position.coords.longitude),
		// 			L.latLng(lat, lon)
		// 		]
		// 	}).addTo(carte);
		// }, (err) => console.warn(`Error generating directions : error code ${err.code} => ${err.message}`))
	}
}




