const CACHE = "pwabuilder-page";

const offlineFallbackPage = "offline.html";
const urlsToCache = [
    "/offline.html",
    "style.css",
    "numero.css",
    "image/coeur.png",
    "logo/accept.jpg",
    "logo/azimut.jpeg",
    "logo/115.png",
    "logo/samu.png",
    "logo/europe.jpeg",
    "logo/pompier.png",
    "logo/police.png",
    "logo/femmes.jpeg",
    "logo/enfant.jpeg",
    "logo/maltraitance.jpeg",
    "logo/poison.png",
    "logo/sms.jpeg",
    "favicon.ico"

];

// Install stage sets up the offline page in the cache and opens a new cache
self.addEventListener("install", function (event) {
    console.log("[PWA Builder] Install Event processing");

    event.waitUntil(
        caches.open(CACHE).then(function (cache) {
            console.log("[PWA Builder] Cached offline page during install");
            return cache.addAll(urlsToCache);
        })
    );
});

// If any fetch fails, it will show the offline page.
self.addEventListener("fetch", function (event) {
    if (event.request.method !== "GET") return;

    event.respondWith(
        fetch(event.request).catch(function (error) {
            // The following validates that the request was for a navigation to a new document
            // if (
            //     event.request.destination !== "document" ||
            //     event.request.mode !== "navigate"
            // ) {
            //     return;
            // }

            console.error("[PWA Builder] Network request Failed. Serving offline page " + error);

            if (event.request.method === "GET") {
                return caches.open(CACHE).then(function (cache) {
                    if (event.request.headers.get('accept').includes('text/html')) {
                        return cache.match(offlineFallbackPage);
                    }
                    else {
                        return cache.match(event.request.url);
                    }
                })
            }
        })
    )
});

// This is an event that can be fired from your page to tell the SW to update the offline page
self.addEventListener("refreshOffline", function () {
    const offlinePageRequest = new Request(offlineFallbackPage);

    return fetch(offlineFallbackPage).then(function (response) {
        return caches.open(CACHE).then(function (cache) {
            console.log("[PWA Builder] Offline page updated from refreshOffline event: " + response.url);
            return cache.put(offlinePageRequest, response);
        });
    });
});
